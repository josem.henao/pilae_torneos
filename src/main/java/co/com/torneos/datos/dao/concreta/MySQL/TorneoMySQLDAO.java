package co.com.torneos.datos.dao.concreta.MySQL;

import co.com.torneos.datos.dao.concreta.interfaces.ITorneoDAO;
import co.com.torneos.dominio.implementaciones.TorneoDominio;

import java.sql.Connection;
import java.util.List;

public class TorneoMySQLDAO implements ITorneoDAO {

    private final Connection conexion;

    public TorneoMySQLDAO(final Connection conexion) {

        if (!obtenerUtilSQL().conexionEstaAbierta(conexion)) {
            final String mensajeUsuario = "Se ha presentado un problema tratando de obtener de llevar a cabo la operación deseada un tipo de cuenta";
            final String mensajeTecnico = "No es posible crear un TipoCuentaSQLServerDAO con una conexión que no está abierta.";

            throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.DATOS);
        }
        this.conexion = conexion;
    }

    @Override
    public void crear(TorneoDominio torneo) {

    }

    @Override
    public void actualizar(TorneoDominio torneo) {

    }

    @Override
    public void eliminar(int codigo) {

    }

    @Override
    public List<TorneoDominio> consultar(String nombre) {
        return null;
    }

    @Override
    public TorneoDominio consultar(int codigo) {
        return null;
    }
}
