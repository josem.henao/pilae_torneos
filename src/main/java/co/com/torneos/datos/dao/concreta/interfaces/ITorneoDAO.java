package co.com.torneos.datos.dao.concreta.interfaces;

import co.com.torneos.dominio.implementaciones.TorneoDominio;

import java.util.List;

public interface ITorneoDAO {
    void crear (TorneoDominio torneo);
    void actualizar(TorneoDominio torneo);
    void eliminar(int codigo);
    List<TorneoDominio> consultar(String nombre);
    TorneoDominio consultar(int codigo);
}
