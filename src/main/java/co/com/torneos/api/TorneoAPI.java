package co.com.torneos.api;

import co.com.torneos.dto.TorneoDTO;
import co.com.torneos.model.Torneo;
import co.com.torneos.negocio.fachada.concreta.TorneoFachada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/torneos")
public class TorneoAPI {

    @Autowired
    TorneoFachada torneoFachada;


    @RequestMapping(value = "", method = RequestMethod.POST)
    public boolean torneosPost(@RequestBody TorneoDTO torneo) {
        return torneoFachada.crear(torneo);
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.GET)
    public String torneosGet( @PathVariable("codigo") int codigo) {
        return torneoFachada.consultar(codigo);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String torneosGet() {
        return torneoFachada.consultar();
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.PUT)
    public boolean torneosPatch(@PathVariable("codigo") int codigo, @RequestBody TorneoDTO torneo) {
        return torneoFachada.actualizar(torneo);
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.PATCH)
    public boolean torneosPatch(@PathVariable("codigo") int codigo, @RequestBody TorneoDTO torneo) {
        return torneoFachada.actualizar(torneo);
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.DELETE)
    public boolean torneosDelete(@PathVariable("codigo") int codigo) {
        return torneoFachada.eliminar(codigo);
    }

}
