package co.com.torneos.negocio.fachada;

import co.com.torneos.dto.TorneoDTO;
import co.com.torneos.model.Torneo;

import java.util.List;

public interface ITorneoFachada {

    void crear(TorneoDTO torneo);

    void actualizar(TorneoDTO torneo);

    void eliminar(TorneoDTO torneo);

    List<TorneoDTO> consultar();

    TorneoDTO consultar(int codigo);
}
