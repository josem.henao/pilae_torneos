package co.com.torneos.negocio.negocio;

import co.com.torneos.dto.TorneoDTO;

public interface ITorneoNegocio {
    void crear(TorneoDTO torneo);
    void obtener(int codigo);
    void obtener();
    void actualizar(TorneoDTO torneo);
    void actualizar(int codigo);
}
