package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.RegistroPosicionDTO;

public class RegistroPosicionDominio {
    private int codigo;
    private int posicion;
    private EquipoDominio equipo;
    private int puntos;

    public RegistroPosicionDominio(int codigo, int posicion, EquipoDominio equipo, int puntos) {
        this.codigo = codigo;
        this.posicion = posicion;
        this.equipo = equipo;
        this.puntos = puntos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public EquipoDominio getEquipo() {
        return equipo;
    }

    public void setEquipo(EquipoDominio equipo) {
        this.equipo = equipo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public RegistroPosicionDTO getAsRegistroPosicionDTO() {
        return new RegistroPosicionDTO(this.codigo,this.posicion,this.equipo.getAsEquipoDTO(),this.puntos);
    }
}
