package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dominio.ITorneoDominio;
import co.com.torneos.dto.EquipoDTO;
import co.com.torneos.dto.PartidaDTO;
import co.com.torneos.dto.PremioDTO;
import co.com.torneos.dto.TorneoDTO;
import co.com.torneos.excepciones.enumeracion.ExcepcionEnum;
import co.com.torneos.excepciones.excepcion.AplicacionExcepcion;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public final class TorneoDominio implements ITorneoDominio {

    private int codigo;
    private String nombre;
    private String descripcion;
    private List<PremioDominio> premiacion;
    private DeporteDominio subdeporte;
    private List<EquipoDominio> equipos;
    private int administrador; // Código del administrador, solo me interesa saber que tengo un admin
    private int organizador; // Código del Organizador, no me intereza nada mas de él
    private int lugar; // Código de la ciudad, no me intereza nada más
    private List<PartidaDominio> partidas;
    private TablaPosicionesDominio tablaPosiciones;

    public TorneoDominio() {
    }

    public TorneoDominio(int codigo, String nombre, String descripcion, List<PremioDominio> premiacion, DeporteDominio subdeporte, List<EquipoDominio> equipos, int administrador, int organizador, int lugar, List<PartidaDominio> partidas, TablaPosicionesDominio tablaPosiciones) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.premiacion = premiacion;
        this.subdeporte = subdeporte;
        this.equipos = equipos;
        this.administrador = administrador;
        this.organizador = organizador;
        this.lugar = lugar;
        this.partidas = partidas;
        this.tablaPosiciones = tablaPosiciones;
    }

    @Override
    public void asegurarIntegridadCodigo() {
        if (this.getCodigo() <= 0) {
            String mensaje = "el codigo de un torneo debe ser mayor a cero";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
        }
    }

    @Override
    public void asegurarIntegridadNombre() {
        if (this.getNombre() == null || this.getNombre().equals("")){
            String mensaje = "Un torneo debe tener un nombre asignado";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
        }
    }

    @Override
    public void asegurarIntegridadSubdeporte() {
        //Validar que se asegure la integridad de este objeto desde su clase correspondiente
    }

    @Override
    public void asegurarIntegridadEquipos() {
        //Validar que se asegure la integridad de este objeto desde su clase correspondiente
    }

    @Override
    public void asegurarIntegridadAdministrador() {
//        if (this.getAdministrador() == null){
//            String mensaje = "el codigo del Administrador de un torneo no puede ser nulo";
//            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.DOMINIO);
//        }
    }

    @Override
    public void asegurarIntegridadLugar() {

    }

    @Override
    public void asegurarIntegridadPartidas() {
        //Evaluar la necesidad de ser integrado
    }

    @Override
    public void asegurarIntegridadTablaPosiciones() {
        //Evaluar la necesidad de ser integrado
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<PremioDominio> getPremiacion() {
        return premiacion;
    }

    public void setPremiacion(List<PremioDominio> premiacion) {
        this.premiacion = premiacion;
    }

    public DeporteDominio getSubdeporte() {
        return subdeporte;
    }

    public void setSubdeporte(DeporteDominio subdeporte) {
        this.subdeporte = subdeporte;
    }

    public List<EquipoDominio> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<EquipoDominio> equipos) {
        this.equipos = equipos;
    }

    public int getAdministrador() {
        return administrador;
    }

    public void setAdministrador(int administrador) {
        this.administrador = administrador;
    }

    public int getOrganizador() {
        return organizador;
    }

    public void setOrganizador(int organizador) {
        this.organizador = organizador;
    }

    public int getLugar() {
        return lugar;
    }

    public void setLugar(int lugar) {
        this.lugar = lugar;
    }

    public List<PartidaDominio> getPartidas() {
        return partidas;
    }

    public void setPartidas(List<PartidaDominio> partidas) {
        this.partidas = partidas;
    }

    public TablaPosicionesDominio getTablaPosiciones() {
        return tablaPosiciones;
    }

    public void setTablaPosiciones(TablaPosicionesDominio tablaPosiciones) {
        this.tablaPosiciones = tablaPosiciones;
    }

    public TorneoDTO getAsTorneoDTO(){
        List<PremioDTO> premiacion = new ArrayList<>();
        for (PremioDominio p : this.premiacion){
            premiacion.add(p.getAsPremioDTO());
        }

        List<EquipoDTO> equipos = new ArrayList<>();
        for (EquipoDominio e : this.equipos){
            equipos.add(e.getAsEquipoDTO());
        }

        List<PartidaDTO> partidas = new ArrayList<>();
        for (PartidaDominio p : this.partidas){
            partidas.add(p.getAsPartidaDTO());
        }

        return new TorneoDTO(this.codigo,this.nombre,this.descripcion, premiacion, this.subdeporte.getAsDeporteDTO(),equipos,this.administrador,this.organizador,this.lugar,partidas,this.tablaPosiciones.asTablaPosicionesDTO());
    }

}
