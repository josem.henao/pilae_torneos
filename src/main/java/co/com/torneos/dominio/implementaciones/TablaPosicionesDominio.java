package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.RegistroPosicionDTO;
import co.com.torneos.dto.TablaPosicionesDTO;

import java.util.ArrayList;
import java.util.List;

public class TablaPosicionesDominio {
    private int codigo;
    private List<RegistroPosicionDominio> registros;

    public TablaPosicionesDominio() {
    }

    public TablaPosicionesDominio(int codigo, List<RegistroPosicionDominio> registros) {
        this.codigo = codigo;
        this.registros = registros;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<RegistroPosicionDominio> getRegistros() {
        return registros;
    }

    public void setRegistros(List<RegistroPosicionDominio> registros) {
        this.registros = registros;
    }

    public TablaPosicionesDTO asTablaPosicionesDTO() {
        List<RegistroPosicionDTO> registros = new ArrayList<>();
        for (RegistroPosicionDominio rp : this.registros){
            registros.add(rp.getAsRegistroPosicionDTO());
        }
        return new TablaPosicionesDTO(this.codigo, registros);
    }
}
