package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.AmonestacionDTO;

public final class AmonestacionDominio {
    private int codigo;
    private String tipoAmonestacion;
    private JugadorDominio jugador;

    public AmonestacionDominio(int codigo, String tipoAmonestacion, JugadorDominio jugador) {
        this.codigo = codigo;
        this.tipoAmonestacion = tipoAmonestacion;
        this.jugador = jugador;
    }

    public final int getCodigo() {
        return codigo;
    }

    public final void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public final String getTipoAmonestacion() {
        return tipoAmonestacion;
    }

    public final void setTipoAmonestacion(String tipoAmonestacion) {
        this.tipoAmonestacion = tipoAmonestacion;
    }

    public final JugadorDominio getJugador() {
        return jugador;
    }

    public final void setJugador(JugadorDominio jugador) {
        this.jugador = jugador;
    }

    public AmonestacionDTO getAsAmonestacionDTO() {
        return new AmonestacionDTO(this.codigo,this.tipoAmonestacion,this.jugador.getAsJugadorDTO());
    }
}
