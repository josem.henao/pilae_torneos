package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.EquipoDTO;
import co.com.torneos.dto.JugadorDTO;

import java.util.ArrayList;
import java.util.List;

public class EquipoDominio {
    private int codigo;
    private String nombre;
    private DeporteDominio subdeporte;
    private List<JugadorDominio> jugadores;

    public EquipoDominio(int codigo, String nombre, DeporteDominio subdeporte, List<JugadorDominio> jugadores) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.subdeporte = subdeporte;
        this.jugadores = jugadores;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public DeporteDominio getSubdeporte() {
        return subdeporte;
    }

    public void setSubdeporte(DeporteDominio subdeporte) {
        this.subdeporte = subdeporte;
    }

    public List<JugadorDominio> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<JugadorDominio> jugadores) {
        this.jugadores = jugadores;
    }

    public EquipoDTO getAsEquipoDTO() {
        List<JugadorDTO> jugadores = new ArrayList<>();
        for (JugadorDominio j : this.jugadores){
            jugadores.add(j.getAsJugadorDTO());
        }
        return new EquipoDTO(this.codigo,this.nombre,this.subdeporte.getAsDeporteDTO(),jugadores);
    }
}
