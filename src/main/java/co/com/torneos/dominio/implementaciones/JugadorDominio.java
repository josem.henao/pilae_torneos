package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.JugadorDTO;

import java.util.Date;

public class JugadorDominio {
    private int codigo;
    private String nombre;
    private String apellido;
    private int nroDorsal;
    private Date nacimiento;

    public JugadorDominio(int codigo, String nombre, String apellido, int nroDorsal, Date nacimiento) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nroDorsal = nroDorsal;
        this.nacimiento = nacimiento;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getNroDorsal() {
        return nroDorsal;
    }

    public void setNroDorsal(int nroDorsal) {
        this.nroDorsal = nroDorsal;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public JugadorDTO getAsJugadorDTO() {
        return new JugadorDTO(this.codigo,this.nombre,this.apellido,this.nroDorsal,this.nacimiento);
    }
}
