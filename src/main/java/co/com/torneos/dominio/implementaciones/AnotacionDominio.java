package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.AnotacionDTO;

import java.util.Date;

public final class AnotacionDominio {
    private int codigo;
    private JugadorDominio jugador;
    private Date minuto;

    public AnotacionDominio(int codigo, JugadorDominio jugador, Date minuto) {
        this.codigo = codigo;
        this.jugador = jugador;
        this.minuto = minuto;
    }

    public final int getCodigo() {
        return codigo;
    }

    public final void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public final JugadorDominio getJugador() {
        return jugador;
    }

    public final void setJugador(JugadorDominio jugador) {
        this.jugador = jugador;
    }

    public final Date getMinuto() {
        return minuto;
    }

    public final void setMinuto(Date minuto) {
        this.minuto = minuto;
    }

    public AnotacionDTO getAsAnotacionDTO() {
        return new AnotacionDTO(this.codigo,this.jugador.getAsJugadorDTO(),this.minuto);
    }
}
