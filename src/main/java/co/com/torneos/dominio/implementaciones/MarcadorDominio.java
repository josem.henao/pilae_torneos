package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.AnotacionDTO;
import co.com.torneos.dto.MarcadorDTO;

import java.util.ArrayList;
import java.util.List;

public class MarcadorDominio {
    private int codigo;
    private List<AnotacionDominio> anotaciones;

    public MarcadorDominio(int codigo, List<AnotacionDominio> anotaciones) {
        this.codigo = codigo;
        this.anotaciones = anotaciones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<AnotacionDominio> getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(List<AnotacionDominio> anotaciones) {
        this.anotaciones = anotaciones;
    }

    public MarcadorDTO getAsMarcadorDTO() {
        List<AnotacionDTO> anotaciones = new ArrayList<>();
        for (AnotacionDominio a : this.anotaciones){
            anotaciones.add(a.getAsAnotacionDTO());
        }
        return new MarcadorDTO(this.codigo, anotaciones);
    }
}
