package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.GeneroDTO;

public enum GeneroDominio {
    MASCULINO(00000001, "Masculino"),
    FEMENINO(00000002,"Femenino"),
    OTRO(00000003,"Otro");

    private int codigo;
    private String titulo;

    GeneroDominio(int codigo, String titulo) {
        this.codigo = codigo;
        this.titulo = titulo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public GeneroDTO getAsGeneroDTO() {
        GeneroDTO retorno = null;
        switch (this.codigo){
            case (000000001):
                retorno = GeneroDTO.MASCULINO;
                break;
            case (000000002):
                retorno = GeneroDTO.FEMENINO;
                break;
            case (000000003):
                retorno = GeneroDTO.OTRO;
                break;
        }
        return retorno;
    }
}
