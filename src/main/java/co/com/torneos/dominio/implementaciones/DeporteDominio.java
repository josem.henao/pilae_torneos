package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.DeporteDTO;

public class DeporteDominio {
    private int codigo;
    private int nroJugadores;
    private GeneroDominio genero;
    private CategoriaDominio categoria;

    public DeporteDominio(int codigo, int nroJugadores, GeneroDominio genero, CategoriaDominio categoria) {
        this.codigo = codigo;
        this.nroJugadores = nroJugadores;
        this.genero = genero;
        this.categoria = categoria;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getNroJugadores() {
        return nroJugadores;
    }

    public void setNroJugadores(int nroJugadores) {
        this.nroJugadores = nroJugadores;
    }

    public GeneroDominio getGenero() {
        return genero;
    }

    public void setGenero(GeneroDominio genero) {
        this.genero = genero;
    }

    public CategoriaDominio getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaDominio categoria) {
        this.categoria = categoria;
    }

    public DeporteDTO getAsDeporteDTO() {
        return new DeporteDTO(this.codigo,this.nroJugadores,this.genero.getAsGeneroDTO(),this.categoria.getAsCategoriaDTO());
    }
}
