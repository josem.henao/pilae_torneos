package co.com.torneos.dominio.implementaciones;

import co.com.torneos.dto.AmonestacionDTO;
import co.com.torneos.dto.PartidaDTO;

import java.util.ArrayList;
import java.util.List;

public class PartidaDominio {
    private int codigo;
    private EquipoDominio equipo1;
    private EquipoDominio equipo2;
    private MarcadorDominio marcador;
    private List<AmonestacionDominio> amonestaciones;

    public PartidaDominio(int codigo, EquipoDominio equipo1, EquipoDominio equipo2, MarcadorDominio marcador, List<AmonestacionDominio> amonestaciones) {
        this.codigo = codigo;
        this.equipo1 = equipo1;
        this.equipo2 = equipo2;
        this.marcador = marcador;
        this.amonestaciones = amonestaciones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public EquipoDominio getEquipo1() {
        return equipo1;
    }

    public void setEquipo1(EquipoDominio equipo1) {
        this.equipo1 = equipo1;
    }

    public EquipoDominio getEquipo2() {
        return equipo2;
    }

    public void setEquipo2(EquipoDominio equipo2) {
        this.equipo2 = equipo2;
    }

    public MarcadorDominio getMarcador() {
        return marcador;
    }

    public void setMarcador(MarcadorDominio marcador) {
        this.marcador = marcador;
    }

    public List<AmonestacionDominio> getAmonestaciones() {
        return amonestaciones;
    }

    public void setAmonestaciones(List<AmonestacionDominio> amonestaciones) {
        this.amonestaciones = amonestaciones;
    }

    public PartidaDTO getAsPartidaDTO() {
        List<AmonestacionDTO> amonestaciones = new ArrayList<>();
        for (AmonestacionDominio a : this.amonestaciones){
            amonestaciones.add(a.getAsAmonestacionDTO());
        }
        return new PartidaDTO(this.codigo,this.equipo1.getAsEquipoDTO(),this.equipo2.getAsEquipoDTO(),this.marcador.getAsMarcadorDTO(),amonestaciones);
    }
}
