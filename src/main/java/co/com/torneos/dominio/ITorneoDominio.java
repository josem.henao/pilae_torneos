package co.com.torneos.dominio;

public interface ITorneoDominio {

    public void asegurarIntegridadCodigo();
    public void asegurarIntegridadNombre();
    public void asegurarIntegridadSubdeporte();
    public void asegurarIntegridadEquipos();
    public void asegurarIntegridadAdministrador();
    //public void asegurarIntegridadOrganizador();
    public void asegurarIntegridadLugar();
    public void asegurarIntegridadPartidas();
    public void asegurarIntegridadTablaPosiciones();

}
