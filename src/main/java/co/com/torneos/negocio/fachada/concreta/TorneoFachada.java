package co.com.torneos.negocio.fachada.concreta;

import co.com.torneos.dto.TorneoDTO;
import co.com.torneos.excepciones.excepcion.AplicacionExcepcion;
import co.com.torneos.negocio.fachada.ITorneoFachada;
import co.com.torneos.negocio.negocio.concreto.TorneoNegocio;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TorneoFachada implements ITorneoFachada {

    @Override
    public void crear(TorneoDTO torneo) {
        final FactoriaDAO factoria = FactoriaDAO.obtenerFactoria("MYSQL");

        try {
            factoria.iniciarTransaccion();

            final ITorneoNegocio negocio = new TorneoNegocio(factoria);
            negocio.crear(tipoCuenta);

            factoria.confirmarTransaccion();
        } catch (final AplicacionExcepcion excepcion) {
            factoria.cancelarTransaccion();
            throw excepcion;
        } catch (final Exception excepcion) {
            factoria.cancelarTransaccion();
            final String mensajeUsuario = "Se ha presentado un problema tratando de registrar la información del nuevo tipo de cuenta";
            final String mensajeTecnico = "Se ha presentado un problema inesperado tratando de crear el nuevo tipo de cuenta.";
            throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, excepcion, ExcepcionEnum.FACHADA);
        } finally {
            factoria.cerrarConexion();
        }
    }

    @Override
    public void actualizar(TorneoDTO torneo) {

    }

    @Override
    public void eliminar(TorneoDTO torneo) {

    }

    @Override
    public List<TorneoDTO> consultar() {
        return null;
    }

    @Override
    public TorneoDTO consultar(int codigo) {
        return null;
    }
}
