package co.com.torneos.negocio.negocio.ensamblador;

import co.com.torneos.utilitarios.dominio.enumeracion.OperacionEnum;

import java.util.List;

public interface IEnsamblador<Dominio, DTO> {
    DTO ensablarDTO(Dominio objetoDominio);
    Dominio ensamblarDominio(DTO objetoDto, OperacionEnum operacion);
}
