package co.com.torneos.negocio.negocio.ensamblador.concreto;

import co.com.torneos.dominio.implementaciones.TorneoDominio;
import co.com.torneos.dto.TorneoDTO;
import co.com.torneos.excepciones.enumeracion.ExcepcionEnum;
import co.com.torneos.excepciones.excepcion.AplicacionExcepcion;
import co.com.torneos.negocio.negocio.ensamblador.IEnsamblador;
import co.com.torneos.utilitarios.dominio.enumeracion.OperacionEnum;

import static co.com.torneos.utilitarios.objeto.UtilObjeto.obtenerUtilObjeto;

public final class TorneoEnsamblador implements IEnsamblador<TorneoDominio, TorneoDTO> {

    private static final IEnsamblador<TorneoDominio, TorneoDTO> INSTANCIA = new TorneoEnsamblador();

    private TorneoEnsamblador() {
        super();
    }

    public static final IEnsamblador<TorneoDominio, TorneoDTO> obtenerTorneoEnsamblador() {
        return INSTANCIA;
    }

    @Override
    public TorneoDTO ensablarDTO(final TorneoDominio objetoDominio) {
        if (obtenerUtilObjeto().objetoEsNulo(objetoDominio)) {
            final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operación deseada sobre un tipo de cuenta";
            final String mensajeTecnico = "No es posible ensamblar un TorneoDTO con un TorneoDominio nulo.";

            throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
        }
        return objetoDominio.getAsTorneoDTO();
    }

    @Override
    public TorneoDominio ensamblarDominio(TorneoDTO objetoDto, OperacionEnum operacion) {
        if (obtenerUtilObjeto().objetoEsNulo(operacion)) {
            final String mensajeUsuario = "Se ha presentado un problema tratando realizar la operación deseada sobre un tipo de cuenta";
            final String mensajeTecnico = "No es posible ensamblar un TipoCuentaDominio con la operación vacía. La operación es requerida para asegurar la integridad del objeto.";

            throw AplicacionExcepcion.CREAR(mensajeTecnico, mensajeUsuario, ExcepcionEnum.NEGOCIO);
        }
        return objetoDto.getAsTorneoDominio();
    }
}
