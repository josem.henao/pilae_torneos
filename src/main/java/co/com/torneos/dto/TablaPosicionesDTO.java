package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.RegistroPosicionDominio;
import co.com.torneos.dominio.implementaciones.TablaPosicionesDominio;

import java.util.ArrayList;
import java.util.List;

public class TablaPosicionesDTO {
    private int codigo;
    private List<RegistroPosicionDTO> registros;

    public TablaPosicionesDTO() {
    }

    public TablaPosicionesDTO(int codigo, List<RegistroPosicionDTO> registros) {
        this.codigo = codigo;
        this.registros = registros;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<RegistroPosicionDTO> getRegistros() {
        return registros;
    }

    public void setRegistros(List<RegistroPosicionDTO> registros) {
        this.registros = registros;
    }

    public TablaPosicionesDominio getAsTablaPosicionesDominio(){
        List<RegistroPosicionDominio> registroPosiciones = new ArrayList<>();
        for(RegistroPosicionDTO rp : this.registros){
            registroPosiciones.add(rp.getAsRegistroPosicionDominio());
        }
        return new TablaPosicionesDominio(this.codigo,registroPosiciones);
    }
}
