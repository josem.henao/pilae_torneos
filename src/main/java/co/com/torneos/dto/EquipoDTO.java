package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.EquipoDominio;
import co.com.torneos.dominio.implementaciones.JugadorDominio;

import java.util.ArrayList;
import java.util.List;

public class EquipoDTO {
    private int codigo;
    private String nombre;
    private DeporteDTO subdeporte;
    private List<JugadorDTO> jugadores;

    public EquipoDTO(int codigo, String nombre, DeporteDTO subdeporte, List<JugadorDTO> jugadores) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.subdeporte = subdeporte;
        this.jugadores = jugadores;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public DeporteDTO getSubdeporte() {
        return subdeporte;
    }

    public void setSubdeporte(DeporteDTO subdeporte) {
        this.subdeporte = subdeporte;
    }

    public List<JugadorDTO> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<JugadorDTO> jugadores) {
        this.jugadores = jugadores;
    }

    public EquipoDominio getAsEquipoDominio(){
        List<JugadorDominio> jugadores = new ArrayList<JugadorDominio>();
        for (JugadorDTO jd : this.jugadores){
            jugadores.add(jd.getAsJugadorDominio());
        }
        return new EquipoDominio(this.codigo,this.nombre,this.subdeporte.getAsSubdeporteDominio(),jugadores);
    }

}
