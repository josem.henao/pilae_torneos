package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.AnotacionDominio;

import java.util.Date;

public final class AnotacionDTO {
    private int codigo;
    private JugadorDTO jugador;
    private Date minuto;

    public AnotacionDTO(int codigo, JugadorDTO jugador, Date minuto) {
        this.codigo = codigo;
        this.jugador = jugador;
        this.minuto = minuto;
    }

    public final int getCodigo() {
        return codigo;
    }

    public final void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public final JugadorDTO getJugador() {
        return jugador;
    }

    public final void setJugador(JugadorDTO jugador) {
        this.jugador = jugador;
    }

    public final Date getMinuto() {
        return minuto;
    }

    public final void setMinuto(Date minuto) {
        this.minuto = minuto;
    }

    public AnotacionDominio getAsAnotacionDominio() {
        return new AnotacionDominio(this.codigo,this.jugador.getAsJugadorDominio(), this.minuto);
    }
}
