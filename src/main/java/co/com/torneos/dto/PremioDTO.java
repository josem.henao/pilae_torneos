package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.PremioDominio;

public class PremioDTO {
    private int codigo;
    private String titulo;
    private String descripcion;

    public PremioDTO(int codigo, String titulo, String descripcion) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PremioDominio getAsPremioDominio(){
        return new PremioDominio(this.codigo,this.titulo,this.descripcion);
    }
}
