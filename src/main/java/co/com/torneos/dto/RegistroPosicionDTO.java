package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.RegistroPosicionDominio;

public class RegistroPosicionDTO {
    private int codigo;
    private int posicion;
    private EquipoDTO equipo;
    private int puntos;

    public RegistroPosicionDTO(int codigo, int posicion, EquipoDTO equipo, int puntos) {
        this.codigo = codigo;
        this.posicion = posicion;
        this.equipo = equipo;
        this.puntos = puntos;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public EquipoDTO getEquipo() {
        return equipo;
    }

    public void setEquipo(EquipoDTO equipo) {
        this.equipo = equipo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public RegistroPosicionDominio getAsRegistroPosicionDominio() {
        return new RegistroPosicionDominio(this.codigo,this.posicion,this.equipo.getAsEquipoDominio(),this.puntos);
    }
}
