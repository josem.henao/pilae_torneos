package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.AmonestacionDominio;

public final class AmonestacionDTO {
    private int codigo;
    private String tipoAmonestacion;
    private JugadorDTO jugador;

    public AmonestacionDTO(int codigo, String tipoAmonestacion, JugadorDTO jugador) {
        this.codigo = codigo;
        this.tipoAmonestacion = tipoAmonestacion;
        this.jugador = jugador;
    }

    public final int getCodigo() {
        return codigo;
    }

    public final void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public final String getTipoAmonestacion() {
        return tipoAmonestacion;
    }

    public final void setTipoAmonestacion(String tipoAmonestacion) {
        this.tipoAmonestacion = tipoAmonestacion;
    }

    public final JugadorDTO getJugador() {
        return jugador;
    }

    public final void setJugador(JugadorDTO jugador) {
        this.jugador = jugador;
    }

    public AmonestacionDominio getAsAmonestacionDominio(){
        return new AmonestacionDominio(this.codigo,this.tipoAmonestacion,this.jugador.getAsJugadorDominio());
    }
}
