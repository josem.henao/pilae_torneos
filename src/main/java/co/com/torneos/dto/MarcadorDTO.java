package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.AnotacionDominio;
import co.com.torneos.dominio.implementaciones.MarcadorDominio;

import java.util.ArrayList;
import java.util.List;

public class MarcadorDTO {
    private int codigo;
    private List<AnotacionDTO> anotaciones;

    public MarcadorDTO(int codigo, List<AnotacionDTO> anotaciones) {
        this.codigo = codigo;
        this.anotaciones = anotaciones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<AnotacionDTO> getAnotaciones() {
        return anotaciones;
    }

    public void setAnotaciones(List<AnotacionDTO> anotaciones) {
        this.anotaciones = anotaciones;
    }

    public MarcadorDominio getAsMarcadorDominio() {
        List<AnotacionDominio> anotaciones = new ArrayList<>();
        for (AnotacionDTO a : this.anotaciones) {
            anotaciones.add(a.getAsAnotacionDominio());
        }
        return new MarcadorDominio(this.codigo,anotaciones);
    }
}
