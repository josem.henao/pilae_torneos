package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.GeneroDominio;

public enum GeneroDTO {
    MASCULINO(00000001, "Masculino"),
    FEMENINO(000000002,"Femenino"),
    OTRO(00000003,"Otro");

    private int codigo;
    private String titulo;

    GeneroDTO(int codigo, String titulo) {
        this.codigo = codigo;
        this.titulo = titulo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public GeneroDominio getAsGeneroDominio() {
        GeneroDominio retorno = null;
        switch (this.codigo){
            case (000000001):
                retorno = GeneroDominio.MASCULINO;
                break;
            case (000000002):
                retorno = GeneroDominio.FEMENINO;
                break;
            case (000000003):
                retorno = GeneroDominio.OTRO;
                break;
        }
        return retorno;
    }
}
