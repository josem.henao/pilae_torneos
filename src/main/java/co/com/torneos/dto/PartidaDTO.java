package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.AmonestacionDominio;
import co.com.torneos.dominio.implementaciones.PartidaDominio;

import java.util.ArrayList;
import java.util.List;

public class PartidaDTO {
    private int codigo;
    private EquipoDTO equipo1;
    private EquipoDTO equipo2;
    private MarcadorDTO marcador;
    private List<AmonestacionDTO> amonestaciones;

    public PartidaDTO(int codigo, EquipoDTO equipo1, EquipoDTO equipo2, MarcadorDTO marcador, List<AmonestacionDTO> amonestaciones) {
        this.codigo = codigo;
        this.equipo1 = equipo1;
        this.equipo2 = equipo2;
        this.marcador = marcador;
        this.amonestaciones = amonestaciones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public EquipoDTO getEquipo1() {
        return equipo1;
    }

    public void setEquipo1(EquipoDTO equipo1) {
        this.equipo1 = equipo1;
    }

    public EquipoDTO getEquipo2() {
        return equipo2;
    }

    public void setEquipo2(EquipoDTO equipo2) {
        this.equipo2 = equipo2;
    }

    public MarcadorDTO getMarcador() {
        return marcador;
    }

    public void setMarcador(MarcadorDTO marcador) {
        this.marcador = marcador;
    }

    public List<AmonestacionDTO> getAmonestaciones() {
        return amonestaciones;
    }

    public void setAmonestaciones(List<AmonestacionDTO> amonestaciones) {
        this.amonestaciones = amonestaciones;
    }

    public PartidaDominio getAsPartidaDominio(){
        List<AmonestacionDominio> amonestaciones = new ArrayList<>();
        for(AmonestacionDTO a : this.amonestaciones){
            amonestaciones.add(a.getAsAmonestacionDominio());
        }
        return new PartidaDominio(this.codigo, this.equipo1.getAsEquipoDominio(),this.equipo2.getAsEquipoDominio(),this.marcador.getAsMarcadorDominio(),amonestaciones);
    }
}
