package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.EquipoDominio;
import co.com.torneos.dominio.implementaciones.PartidaDominio;
import co.com.torneos.dominio.implementaciones.PremioDominio;
import co.com.torneos.dominio.implementaciones.TorneoDominio;

import java.util.ArrayList;
import java.util.List;

public final class TorneoDTO {

    private int codigo;
    private String nombre;
    private String descripcion;
    private List<PremioDTO> premiacion;
    private DeporteDTO subdeporte;
    private List<EquipoDTO> equipos;
    private int administrador; // Código del administrador, solo me interesa saber que tengo un admin
    private int organizador; // Código del Organizador, no me intereza nada mas de él
    private int lugar; // Código de la ciudad, no me intereza nada más
    private List<PartidaDTO> partidas;
    private TablaPosicionesDTO tablaPosiciones;


    public TorneoDTO(int codigo, String nombre, String descripcion, List<PremioDTO> premiacion, DeporteDTO subdeporte, List<EquipoDTO> equipos, int administrador, int organizador, int lugar, List<PartidaDTO> partidas, TablaPosicionesDTO tablaPosiciones) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.premiacion = premiacion;
        this.subdeporte = subdeporte;
        this.equipos = equipos;
        this.administrador = administrador;
        this.organizador = organizador;
        this.lugar = lugar;
        this.partidas = partidas;
        this.tablaPosiciones = tablaPosiciones;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<PremioDTO> getPremiacion() {
        return premiacion;
    }

    public void setPremiacion(List<PremioDTO> premiacion) {
        this.premiacion = premiacion;
    }

    public DeporteDTO getSubdeporte() {
        return subdeporte;
    }

    public void setSubdeporte(DeporteDTO subdeporte) {
        this.subdeporte = subdeporte;
    }

    public List<EquipoDTO> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<EquipoDTO> equipos) {
        this.equipos = equipos;
    }

    public int getAdministrador() {
        return administrador;
    }

    public void setAdministrador(int administrador) {
        this.administrador = administrador;
    }

    public int getOrganizador() {
        return organizador;
    }

    public void setOrganizador(int organizador) {
        this.organizador = organizador;
    }

    public int getLugar() {
        return lugar;
    }

    public void setLugar(int lugar) {
        this.lugar = lugar;
    }

    public List<PartidaDTO> getPartidas() {
        return partidas;
    }

    public void setPartidas(List<PartidaDTO> partidas) {
        this.partidas = partidas;
    }

    public TablaPosicionesDTO getTablaPosiciones() {
        return tablaPosiciones;
    }

    public void setTablaPosiciones(TablaPosicionesDTO tablaPosiciones) {
        this.tablaPosiciones = tablaPosiciones;
    }

    public TorneoDominio getAsTorneoDominio(){
        List<PremioDominio> premiacion = new ArrayList<>();
        for (PremioDTO p : this.premiacion){
            premiacion.add(p.getAsPremioDominio());
        }

        List<EquipoDominio> equipos = new ArrayList<>();
        for (EquipoDTO e : this.equipos){
            equipos.add(e.getAsEquipoDominio());
        }

        List<PartidaDominio> partidas = new ArrayList<>();
        for (PartidaDTO p : this.partidas){
            partidas.add(p.getAsPartidaDominio());
        }

        return new TorneoDominio(this.codigo,this.nombre,this.descripcion,premiacion,this.subdeporte.getAsSubdeporteDominio(),equipos,this.administrador,this.organizador,this.lugar, partidas,this.tablaPosiciones.getAsTablaPosicionesDominio());
    }
}
