package co.com.torneos.dto;

import co.com.torneos.dominio.implementaciones.DeporteDominio;

public class DeporteDTO {
    private int codigo;
    private int nroJugadores;
    private GeneroDTO genero;
    private CategoriaDTO categoria;

    public DeporteDTO(int codigo, int nroJugadores, GeneroDTO genero, CategoriaDTO categoria) {
        this.codigo = codigo;
        this.nroJugadores = nroJugadores;
        this.genero = genero;
        this.categoria = categoria;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getNroJugadores() {
        return nroJugadores;
    }

    public void setNroJugadores(int nroJugadores) {
        this.nroJugadores = nroJugadores;
    }

    public GeneroDTO getGenero() {
        return genero;
    }

    public void setGenero(GeneroDTO genero) {
        this.genero = genero;
    }

    public CategoriaDTO getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaDTO categoria) {
        this.categoria = categoria;
    }

    public DeporteDominio getAsSubdeporteDominio(){
        return new DeporteDominio(this.codigo,this.nroJugadores, this.genero.getAsGeneroDominio(), this.categoria.asCategoriaDominio());
    }
}
