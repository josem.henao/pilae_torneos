package co.com.torneos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorneosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TorneosApplication.class, args);
	}
}
